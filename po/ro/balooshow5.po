# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2014, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-02 00:41+0000\n"
"PO-Revision-Date: 2022-12-28 13:52+0000\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sergiu Bivol"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sergiu@cip.md"

#: main.cpp:57
#, kde-format
msgid "Baloo Show"
msgstr "Baloo Show"

#: main.cpp:59
#, kde-format
msgid "The Baloo data Viewer - A debugging tool"
msgstr "Vizualizator de date Baloo - Unealtă de depanare"

#: main.cpp:61
#, kde-format
msgid "(c) 2012, Vishesh Handa"
msgstr "(c) 2012, Vishesh Handa"

#: main.cpp:66
#, kde-format
msgid "Urls, document ids or inodes of the files"
msgstr ""

#: main.cpp:68
#, kde-format
msgid "Print internal info"
msgstr ""

#: main.cpp:70
#, kde-format
msgid "Arguments are interpreted as inode numbers (requires -d)"
msgstr ""

#: main.cpp:72
#, kde-format
msgid "Device id for the files"
msgstr ""

#: main.cpp:95
#, kde-format
msgid "Error: -i requires specifying a device (-d <deviceId>)"
msgstr ""

#: main.cpp:101
#, kde-format
msgid ""
"The Baloo index could not be opened. Please run \"%1\" to see if Baloo is "
"enabled and working."
msgstr ""

#: main.cpp:118
#, kde-format
msgid "The document IDs of the Baloo DB and the filesystem are different:"
msgstr ""

#: main.cpp:137
#, kde-format
msgid "%1: Not a valid url or document id"
msgstr ""

#: main.cpp:151
#, kde-format
msgid "%1: Failed to parse inode number"
msgstr ""

#: main.cpp:175
#, kde-format
msgid "%1: No index information found"
msgstr ""

#: main.cpp:223
#, kde-format
msgid "Internal Info"
msgstr "Informație internă"

#: main.cpp:224
#, kde-format
msgid "Terms: %1"
msgstr "Termeni: %1"

#: main.cpp:225
#, kde-format
msgid "File Name Terms: %1"
msgstr "Termeni denumire de fișier: %1"

#: main.cpp:226
#, kde-format
msgid "%1 Terms: %2"
msgstr "Termeni %1: %2"

#: main.cpp:229
#, kde-format
msgctxt "Prefix string for internal errors"
msgid "Internal Error - %1"
msgstr "Eroare internă - %1"

#~ msgid "Vishesh Handa"
#~ msgstr "Vishesh Handa"

#~ msgid "Maintainer"
#~ msgstr "Responsabil"
